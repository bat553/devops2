# Day3 - PELLARIN

## One pod
```
# GET _cat/health?v
epoch      timestamp cluster       status node.total node.data shards pri relo init unassign pending_tasks max_task_wait_time active_shards_percent
1669796778 08:26:18  elasticsearch green           1         1     10  10    0    0        0             0                  -                100.0%

# GET _cat/allocation?v
shards disk.indices disk.used disk.avail disk.total disk.percent host       ip         node
    10       41.9mb    62.3mb      911mb    973.4mb            6 10.0.3.109 10.0.3.109 elasticsearch-es-default-0
```

## Three pods
```
# GET _cat/health?v
epoch      timestamp cluster       status node.total node.data shards pri relo init unassign pending_tasks max_task_wait_time active_shards_percent
1669797275 08:34:35  elasticsearch green           3         3     20  10    2    0        0             1                  -                100.0%

# GET _cat/allocation?v
shards disk.indices disk.used disk.avail disk.total disk.percent host       ip         node
     9       44.2mb    61.6mb    911.7mb    973.4mb            6 10.0.7.71  10.0.7.71  elasticsearch-es-default-1
    10         42mb    62.6mb    910.7mb    973.4mb            6 10.0.3.109 10.0.3.109 elasticsearch-es-default-0
     1        2.4mb    18.7mb    954.6mb    973.4mb            1 10.0.5.69  10.0.5.69  elasticsearch-es-default-2
```

## Que constatez-vous ?
`On a trois noeuds`

## Pour vérifier que tout fonctionne, essayez de détruire un deployment manuellement dans votre Cluster. Que se passe-t-il ?
`On voit une alterte sur ArgoCD puis, celui-ci le recréé`

## Essayez de modifier le values.yaml en augmentant le replicaCount par exemple. Que se passe-t-il ?
`Le service est mis à jour avec le bon nombre de pods automatiquement`
## DeamonSet 
`Déploiement sur chaque noeuds (utile pour un node_exporter)`

