# Day2

## Quel est le nom du service de la base de données ?
`pg-service`

## Pourquoi le computer a disparu ?
`Parce qu'il n'y a pas de persistence des données`

## D’après le tableau, quel est le type d’accès implémenté par notre Storage Class EBS ? Pourquoi cela convient parfaitement pour la persistance de la base de données Postgres ?
`ReadWriteOnce / Pour évtier les écritures en concurence`

## Vérifiez que le PVC est créé avec le PV. Quel est le nom du PV ?
