# Day 1 - PELLARIN - Devops2

## Quelles sont les informations que l'on retrouve dans ce fichier ?
`On retrouve les certificats et clés privée pour s'authentifier auprès au dserveur ainsi que l'URL vers le kubeAPI`

## Quelle est la différence (namepsace) ?
`Sur la première version on regarde dans mon namespace et la deuxième on regarde dans un namespace auquel on a pas accès`

## Quelles sont les propriétés principales que l'on retrouve (pod) ?
`On retrouve : l'image, le namespace, les limites de ressources, les différents volumes...`

## Que remarquez-vous dans la description des properties spec: template ? À quoi sert le selector: matchLabels ?
`Le template permet de créer plusieurs pods `

## Combien y a-t'il de pods déployés dans votre namespace ?
`3`

## Que se passe-t'il ?
`La configuration prédédente a déployé 3 replicaSet avec chacun un pods`

## Que se passe-t'il ?
`Les pods sont supprimés en même temps que les ReplicaSet`

## Quels sont les changements par rapport au ReplicaSet ?
`A première vue il n'y a pas de grandes différences hormis le 'kind'`

## Combien y a-t'il de ReplicaSet ? De Pods ?
`Il y a un replicaSet et 3 pods`

## Une fois terminé, combien y a-t-il de replicaset ? Combien y a-t-il de Pods ? Allez voir les logs des événements du déploiement avec kubectl describe deployments. Qu’observez vous ?
`Il y a deus replicaSet (dont un avec zero pods). 3 pods. On voit qu'il a décrémenté l'ancien replicaset et incrémenté proportionnement le nouveau`

## Que se passe-t'il ? Pourquoi ?
`La nouvelle image n'existant pas, k8s ne décrémente pas l'ancien replicaSet tant que le nouveau ne fonctionne pas.`

## Combien y a-t-il de révisions ? À quoi correspond le champ CHANGE-CAUSE ?
`3 révisions. Ce champs sert à expliciter ce qu'il s'est passé pour enclancher ce changement`

## Combien y a-t'il de Pods?
`5`

## Que se passe-t-il au niveau ReplicaSet ?
`Il ne se passe rien`

## Que se passe-t-il au niveau ReplicaSet ?
`Le déploiement du nouveau replicaset est en cours`

# Step2

## Que se passe-t'il ? Pourquoi ? [Container]
`Le service n'arrive pas à récupérer l'image OCI, celui-ci demande des identifiants`

## Décrivez ce que répond la Web App ? Actualisez votre page avec CTRL + F5. Que se passe-t-il ?
`'Je suis le Pod'. La couleur du fond change`

## Que constatez-vous sur le navigateur ?
`On voit l'ip du pod et son nom`

## 

